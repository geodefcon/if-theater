//
//  SupervisorVC.swift
//  mySlideOutMenu
//
//  Created by user on 26/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit
import Kanna
import AlamofireImage

// This View Controller perform work of three virtual VC
// and treats three menu's items:
// 1 - Administrations
// 2 - Actors
// 3 - Art chiefs 

class SupervisorVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var superTableView: UITableView!
    var LINKS = [String]()
    var supervisors = [Int:[Supervisor]]() // Each VC - has own array of person
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var tab: Int = 3
    var defaultLink: String = ""
    let firstIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        supervisors[2] = [Supervisor]()
        supervisors[3] = [Supervisor]()
        supervisors[4] = [Supervisor]()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SupervisorVC.change), name:"menuDidSelected", object: nil)
        
        self.tabBarController?.tabBar.hidden = true
        
        superTableView.dataSource = self
        superTableView.delegate = self
        
       
    }
    
    func change(notification: NSNotification) {
        self.tab = notification.object as! Int
        
        if (self.tab >= 2 && self.tab <= 4) {
            switch self.tab {
            case 2:
                defaultLink = "http://dramteatr.if.ua/category&category_id=13"
            case 3:
                defaultLink = "http://dramteatr.if.ua/category&category_id=14"
            case 4:
                defaultLink = "http://dramteatr.if.ua/category&category_id=16"
            default:
                break
            }
            self.LINKS = [String]()
            parseSite()
            self.superTableView.reloadData()
            
            // When change virtual VC, we are going to top table
            if supervisors[tab]!.count > 0 {
                self.superTableView.selectRowAtIndexPath(firstIndexPath, animated: false, scrollPosition: .Top)
                self.superTableView.deselectRowAtIndexPath(firstIndexPath, animated: false)
            }
            
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supervisors[tab]!.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var imgHeight: CGFloat
        var imgWidth: CGFloat
        var margin: CGFloat
        
        let supervisor = supervisors[tab]![indexPath.row]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("SuperCell", forIndexPath: indexPath) as? SuperCell {
            cell.personImg.image = nil
            cell.configureCell(supervisor)
            
            cell.backgroundView = UIImageView(image: UIImage(named: "menu_bg"))//"bg_cell.jpg"))
            cell.backgroundView?.alpha = 0.6
            cell.backgroundView?.contentMode = .ScaleAspectFill
            
            let url = supervisor.imgPath!
            let escapedUrl = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            let nsUrl = NSURL(string: escapedUrl!)
            
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                imgHeight = 350// Ipad
                imgWidth = 250
            } else {
                margin = 30.0
                imgHeight = 400
                imgWidth = screenSize.width - 2 * margin// Iphone
            }
            
            let filter = AspectScaledToFitSizeFilter(
                size: CGSize(width: imgWidth, height: imgHeight)
            )
 
            cell.personImg.af_setImageWithURL(
                nsUrl!,
                filter: filter
            )
            return cell
            
        } else {
            let cell = SuperCell()
            cell.configureCell(supervisor)
            return cell
        }
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
            return 370.0// Ipad
        } else {
            return 450.0// Iphone
        }
    }
    
    
    func parseSite(var urlString: String = "") -> String? {
  
        if urlString == "" {
            urlString = self.defaultLink
        }
        if let url = NSURL(string: urlString) {
            var myHtmlString: NSString?
            
            do {
                myHtmlString = try NSString(contentsOfURL: url, encoding: NSUTF8StringEncoding)
            } catch _ {
                NSNotificationCenter.defaultCenter().postNotificationName("checkWeb", object: nil)
                return ""
            }
            
            
            if let doc = HTML(html: myHtmlString as! String,  encoding: NSUTF8StringEncoding){
                if (LINKS.count == 0) {
                    for pageBlock in doc.css("div") where (pageBlock.className == "pagination"){
                        
                        for li in pageBlock.css("li") {
                            if let number = Int(li.text!) {
                                LINKS.append(self.defaultLink + "&page=\(number)")
                            }
                        }
                        supervisors[tab] = [Supervisor]()
                        
                        for url in LINKS {
                            parseSite(url)
                        }
                        return nil
                    }
                }
                // if page doesn't have div with classname "pagination"
                if (LINKS.count == 0) {
                    supervisors[tab] = [Supervisor]()
                }
                //
                var i = 0
                
                for article in doc.css("article") {
    
                    var checkContent = false
                    var newImgPath: String = ""
                    //var head: String?
                    var position: String?
                    var name: String
                    
                    let heads = article.css("a")
                    
                    if let head = heads[0].text {
                        name = head
                    } else {
                        name = ""
                    }
                    
                    let divs = article.css("div")
                    
                    for div in divs {
                        if (div.className == "content") && checkContent == false {
                            checkContent = true
                            
                            if let img = div.css("img")[0]["src"] {
                                let imgPath = "http://dramteatr.if.ua" + img
                                newImgPath = imgPath.stringByReplacingOccurrencesOfString("thumb_", withString: "")
                            }
                            
                            let titles = div.css("p")
                            
                            if titles.count > 0 {
                                if let text0 = titles[0].text  {
                                    position = text0
                                    
                                    if text0 == "" {
                                        if let text1 = titles[1].text {
                                            position = text1
                                        
                                            if text1 == "" {
                                                position = ""
                                            }
                                        }
                                    }
                                }
                            } else {
                                position = ""
                            }
                            // Problem with not closed tags - cannot parse correct data
                            switch name {
                            case "Курілець Богдан Михайлович":
                                position = "Головний диригент"
                            case "Бабинська Ганна Василівна ":
                                position = "Провідний майстер сцени"
                            case "Ніколаєнко Любов Михайлівна":
                                position = "Провідний майстер сцени"
                            case " Вітушинський Віктор Антонович":
                                position = "Провідний майстер сцени"
                            case "Пасічняк Олеся Василівна":
                                position = "Артистка драми"
                            case "Гірник Тетяна":
                                position = "Артистка драми"
                            case "Левицький Ігор Романович ":
                                position = "Перша флейта"
                            case "Нагірна Наталія Михайлівна":
                                position = "Друга флейта"
                            case "Гричко Роман Богданович ":
                                position = "Перший кларнет"
                            case "Зрайко Ігор Богданович":
                                position = "Артист оркестру театру з 2002 року"
                            case "Ількович Василь Миколайович":
                                position = "Перша валторна"
                            case "Мельниченко Любов Василівна":
                                position = "Перша скрипка"
                            case "Олексієнко Валентин Степанович":
                                position = "Перша скрипка"
                            case "Лущак Маргарита Василівна":
                                position = "Друга скрипка"
                            case "Пасечко Андрій Михайлович":
                                position = "Друга скрипка"
                            default:
                                break
                            }
                            
                            let supervisor = Supervisor(name: name, position: position!, imgPath: newImgPath)
                            i += 1
                            supervisors[tab]?.append(supervisor)
                        }
                    }
                }
            }
            
        } else {
            print("url not exist")
        }
        return nil
    }
}
