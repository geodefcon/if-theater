//
//  TheaterAnnotation.swift
//  mySlideOutMenu
//
//  Created by user on 29/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation
import MapKit

class TheaterAnnotation: NSObject, MKAnnotation {
    
    var coordinate = CLLocationCoordinate2D()
    
    init(coordinate: CLLocationCoordinate2D){
        self.coordinate = coordinate
    }
}