//
//  AfishaVC.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit
import Kanna
import AlamofireImage

class AfishaVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var afishaTableView: UITableView!
    var posts = [Post]()
    var LINKS = [String]()
    var CELL_HEIGHT: CGFloat!
    var CELL_WIDTH: CGFloat!
    var size: CGSize!
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.hidden = true
        afishaTableView.dataSource = self
        afishaTableView.delegate = self
        afishaTableView.rowHeight = UITableViewAutomaticDimension
        afishaTableView.estimatedRowHeight = 400.0
    }
    
    override func viewDidAppear(animated: Bool) {
        LINKS = [String]()
        parseSite()
        afishaTableView.reloadData()
        afishaTableView.layoutSubviews()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let post = posts[indexPath.row]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("PostCell", forIndexPath: indexPath) as? PostCell {
            cell.myImgView.image = nil
            cell.configureCell(post)
        
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                // Ipad
                let margin: CGFloat = 32.0
                CELL_HEIGHT = 400.0
                CELL_WIDTH = screenSize.width - margin * 2
            }
            else {
                // Iphone
                let margin: CGFloat = 8.0
                CELL_HEIGHT = 200.0
                CELL_WIDTH = screenSize.width - margin * 2
            }
            cell.backgroundView = UIImageView(image: UIImage(named: "menu_bg"))
            cell.backgroundView?.alpha = 0.6
            cell.backgroundView?.contentMode = .ScaleToFill
     
            let url = post.imgPath!
            let escapedUrl = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
            let nsUrl = NSURL(string: escapedUrl!)
            
            let filter = AspectScaledToFillSizeFilter(
                size: CGSize(width: CELL_WIDTH, height: CELL_HEIGHT)
            )
            
            cell.myImgView.af_setImageWithURL(
                nsUrl!,
                filter: filter
            )
            return cell
            
        } else {
            let cell = PostCell()
            cell.configureCell(post)
            return cell
        }
    }
    
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
//            return 540.0// Ipad
//        } else {
//            return 300.0// Iphone
//        }
//    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
//    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let post = posts[indexPath.row]
        performSegueWithIdentifier("postDetailVC", sender: post)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "postDetailVC" {
            if let detailsVC = segue.destinationViewController as? postDetailVC {
                if let post = sender as? Post {
                    
                    detailsVC.post = post
                }
            }
        }
    }

    
    func parseSite(urlString: String = "http://dramteatr.if.ua/category&category_id=6")->String?{
        
        if let url = NSURL(string: urlString) {
            var myHtmlString: NSString?
            
            do {
                myHtmlString = try NSString(contentsOfURL: url, encoding: NSUTF8StringEncoding)
            } catch _ {
                //print("check internet connection!")
                NSNotificationCenter.defaultCenter().postNotificationName("checkWeb", object: nil)
                return ""
            }

            if let doc = HTML(html: myHtmlString as! String,  encoding: NSUTF8StringEncoding){
                
                if (LINKS.count == 0) {
                    posts = [Post]()
                    for pageBlock in doc.css("div") where (pageBlock.className == "pagination"){
                        
                        for li in pageBlock.css("li") {
                            if let number = Int(li.text!) {
                                LINKS.append("http://dramteatr.if.ua/category&category_id=6&page=\(number)")
                            }
                        }
                        
                        for url in LINKS {
                            parseSite(url)
                        }
                        return nil
                    }
                }
                
                for article in doc.css("article") {
                    
                    var post: Post
                    var imgPath: String = ""
                    var newImgPath: String = ""
                    var dw = ""
                    var dm = ""
                    var time = ""
                    
                    let divs = article.css("div")
                    
                    for div in divs {
                        if (div.className == "date") {
                            let spans = div.css("span")
                            
                            for span in spans {
                                if span.className == "dw"{
                                    dw = span.text! // day of the week
                                }
                                if span.className == "dm"{
                                    dm = span.text! // day of the month
                                }
                                if span.className == "time"{
                                    
                                    let t = span.text!
                                    let start = t.endIndex.advancedBy(-5)
                                    let end = t.endIndex
                                    time = t[start..<end]
                                }
                            }
                        }
                        if (div.className == "content") {
                            
                            if let img = div.css("img")[0]["src"] {
                                imgPath = "http://dramteatr.if.ua" + img
                                newImgPath = imgPath.stringByReplacingOccurrencesOfString("thumb_", withString: "")
                            }
                            
                            let titles = div.css("h1")
                            let title = titles[0].text!
                            
                            let forwardLinks = titles[0].css("a")
                            let forwardLink = forwardLinks[0]["href"]
                            let link = "http://dramteatr.if.ua\(forwardLink!)"
    
                            post = Post(path: newImgPath, header: title, dm: dm, dw: dw, time: time, link: link)
                            self.posts.append(post)
                        }
                    }
                }
            }
        } else {
            print("url not exist")
        }
        return nil
    }
}
