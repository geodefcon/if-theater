//
//  ContactsVC.swift
//  mySlideOutMenu
//
//  Created by user on 29/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit
import MapKit

class ContactsVC: UIViewController, MKMapViewDelegate {
    
    
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var map: MKMapView!
    
    let locationManager = CLLocationManager()
    var regionRadius: CLLocationDistance {
        get {
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                return 400.0// Ipad
            } else {
                return 800.0// Iphone
            }
        }
    }
    
    
    var theaterCoord = CLLocationCoordinate2D(latitude: 48.917840, longitude: 24.719000)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        self.tabBarController?.tabBar.hidden = true

        centerMapOnLocation()
        setTheaterAnnot()
    }

    
    func centerMapOnLocation() {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(theaterCoord, regionRadius * 2, regionRadius * 2)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKindOfClass(TheaterAnnotation) {
            let annoView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Default")
            annoView.pinTintColor = UIColor.redColor()
            annoView.animatesDrop = true
            
            return annoView
        }
        return nil
    }

    
    func setTheaterAnnot(){
        let theater = TheaterAnnotation(coordinate: theaterCoord)
        map.addAnnotation(theater)
    }
}
