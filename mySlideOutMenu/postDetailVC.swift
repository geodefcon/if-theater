//
//  postDetailVC.swift
//  mySlideOutMenu
//
//  Created by user on 19/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit
import Kanna


class postDetailVC: UIViewController {
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
 
    @IBOutlet weak var text: UILabel!

    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var mainImg: UIImageView!
    
    var post: Post!
    let fontSize: CGFloat = 12
    let regFontName = "HelveticaNeue"
    let boldFontName = "HelveticaNeue-Bold"
    var changedImgViewHeight: Bool = false
    let screenSize: CGRect = UIScreen.mainScreen().bounds


    override func viewDidLoad() {
        super.viewDidLoad()
       
        text.text = parseSite(post.link!)
        
        let url = post.imgPath!
        let escapedUrl = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let nsUrl = NSURL(string: escapedUrl!)
        mainImg.af_setImageWithURL(nsUrl!)
        
        mainImg.clipsToBounds = true
        mainImg.contentMode = UIViewContentMode.ScaleAspectFit
        
        header.text = post.header
        day.text = post.dm!
        time.text = post.dw! + "    " + post.time!
    }

    
    override func viewDidLayoutSubviews() {
        if !changedImgViewHeight {
            let h = self.mainImg.image?.size.height
            let w = self.mainImg.image?.size.width
            
            // Changing imageView size, according  image orientation 
            if w > h {
                self.imgViewHeight.constant = self.imgViewHeight.constant * 0.8
                changedImgViewHeight = true
            }
            if h > w {
                self.imgViewHeight.constant =  self.screenSize.height * 0.6 //self.imgViewHeight.constant *
            }
        }
    }
    
//// for strings with attributes
//    func attrString(string: String, bold: Bool = false) -> NSMutableAttributedString {
//        let Font: UIFont?
//        let attrs: NSDictionary
//        
//        if bold {
//            Font = UIFont(name: boldFontName, size: fontSize)
//        } else {
//            Font = UIFont(name: regFontName, size: fontSize)
//        }
//        attrs = [NSFontAttributeName: Font!] as NSDictionary
//        let resString = NSMutableAttributedString(string:string, attributes:attrs as? [String : AnyObject])
//        
//        return resString
//    }
    
    @IBAction func backBtnPressed(sender: AnyObject) {
        navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func leftSwipe(sender: UISwipeGestureRecognizer) {
        navigationController!.popViewControllerAnimated(true)
    }
    
    func parseSite(urlString: String) -> String? {
        
        if let url = NSURL(string: urlString) {
            var myHtmlString: NSString?
            
            do {
                try myHtmlString = NSString(contentsOfURL: url, encoding: NSUTF8StringEncoding)
            } catch _ {
                NSNotificationCenter.defaultCenter().postNotificationName("checkWeb", object: nil)
                return ""
            }

            var baseString = ""// attrString("")
            //var addString = attrString("Test1", bold: true)
            //string.appendAttributedString(addString)
            
            if let doc = HTML(html: myHtmlString as! String,  encoding: NSUTF8StringEncoding){
                
                for article in doc.css("article") {
                    let divs = article.css("div")
        
                    for div in divs {
                        if (div.className == "content") {
                            let strings = div.css("p")
                            for string in strings {
                                baseString += string.text! + "\n"
// for strings with attributes
                                //let splitString = str.characters.split{$0 == "\n"}.map(String.init)
                                
                                
//                                for element in splitString {
//                                    print(element)
                                    //var addString = attrString(str, bold: false)
                                    //baseString.appendAttributedString(addString)
//                                }
                                //addString = attrString("\n", bold: false)
                                //baseString.appendAttributedString(addString)
                                
                            }
                            return baseString
                        }
                    }
                }
            }
            
            
        } else {
            print("url not exist")
        }
        return nil
    }
}
