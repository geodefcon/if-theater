//
//  MenuTVC.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class MenuTVC: UITableViewController {

    @IBOutlet weak var txtLbl: UILabel!
    
    
    let theData = ["Афіша", "Історія театру", "Художньо-керівний склад", "Творчий склад", "Адміністрація", "Квитки", "Контакти"]
    var tbc: UITabBarController!
    
    override func viewDidAppear(animated: Bool) {
        tbc = self.parentViewController!.childViewControllers[1] as! UITabBarController
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "menu_bg.jpg"))
        self.tableView.backgroundView?.alpha = 0.6
        self.tableView.backgroundView?.contentMode = .ScaleToFill
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theData.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> MenuCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as? MenuCell {
            cell.configure(theData[indexPath.row])
            cell.backgroundColor = UIColor.clearColor();
            return cell
        } else {
            return MenuCell()
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad){
            return 100.0// Ipad
        } else {
            return 50.0// Iphone
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let ind = indexPath.row
        
        switch ind {
        case 0...1:
            tbc.selectedIndex = ind
        case 2...4:
            //NSNotificationCenter.defaultCenter().postNotificationName("menuDidSelected", object: indexPath.row)
            tbc.selectedIndex = 2
            //print("i=",indexPath.row)
        case 5...6:
            tbc.selectedIndex = ind - 2
        default:
            break
        }
        NSNotificationCenter.defaultCenter().postNotificationName("menuDidSelected", object: indexPath.row)
    }

}
