//
//  TicketVC.swift
//  mySlideOutMenu
//
//  Created by user on 01/05/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class TicketVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.hidden = true
    }


    @IBAction func makeCall(sender: UIButton) {
        var phone: String
        
        if sender.tag == 0 {
            phone = "380342752415"
        } else {
            phone = "380342750724"
        }

        let formatedNumber = phone.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
        let phoneUrl = "tel://\(formatedNumber)"
        let url:NSURL = NSURL(string: phoneUrl)!
        UIApplication.sharedApplication().openURL(url)
    }
}