//
//  MenuCell.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    
    @IBOutlet weak var stringLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(text: String){
        self.stringLbl.text = text
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
