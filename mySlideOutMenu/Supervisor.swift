//
//  Supervisor.swift
//  mySlideOutMenu
//
//  Created by user on 26/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation


class Supervisor {
    var name: String?
    var position: String?
    var imgPath: String?
    
    
    init(name: String, position: String, imgPath: String) {
        self.name = name
        self.position = position
        self.imgPath = imgPath
    }
    
}