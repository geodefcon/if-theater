//
//  AboutVC.swift
//  mySlideOutMenu
//
//  Created by user on 23/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit
import Kanna

class AboutVC: UIViewController {

    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var txtLbl: UILabel!
    let historyLink = "http://dramteatr.if.ua/content&content_id=6"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headImageView.clipsToBounds = true
        self.headImageView.contentMode = UIViewContentMode.ScaleAspectFill
        txtLbl.text = parseSite(historyLink)
    }

    
    
    func parseSite(urlString: String) -> String? {
        
        if let url = NSURL(string: urlString) {
            var myHtmlString: NSString?
            
            do {
                try myHtmlString = NSString(contentsOfURL: url, encoding: NSUTF8StringEncoding)
            } catch _ {
                NSNotificationCenter.defaultCenter().postNotificationName("checkWeb", object: nil)
                return ""
            }
            var baseString = ""
          
            if let doc = HTML(html: myHtmlString as! String,  encoding: NSUTF8StringEncoding){
                
                for article in doc.css("article") {
                    let divs = article.css("div")
                    
                    for div in divs {
                        if (div.className == "content") {
                            let strings = div.css("p")
                            
                            for string in strings {
                                baseString += string.text! + "\n"
                            }
                            return baseString
                        }
                    }
                }
            }
        } else {
            print("url not exist")
        }
        return nil
    }
}
