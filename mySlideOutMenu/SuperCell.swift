//
//  SuperCell.swift
//  mySlideOutMenu
//
//  Created by user on 26/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit


class SuperCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var positionLbl: UILabel!
    @IBOutlet weak var personImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        personImg.clipsToBounds = true
        personImg.contentMode = UIViewContentMode.ScaleAspectFit
    }
    

    func configureCell(supervisor: Supervisor) {
        self.nameLbl.text = supervisor.name
        self.positionLbl.text = supervisor.position
    }
}
