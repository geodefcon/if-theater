//
//  Post.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation
import UIKit

class Post {
    var imgPath: String?
    var header: String?
    var dm: String?
    var dw: String?
    var time: String?
    var link: String?
    var postImg: UIImage?
    
    init(path: String, header: String, dm: String, dw: String, time: String, link: String){
        self.imgPath = path
        self.header = header
        self.dm = dm
        self.dw = dw
        self.time = time
        self.link = link
    }
}