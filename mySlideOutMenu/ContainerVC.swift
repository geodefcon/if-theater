//
//  ContainerVC.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var masterView: UIView!
    @IBOutlet weak var navView: UIView!
    
    @IBOutlet weak var menuLbl: UILabel!
    
    @IBOutlet weak var rightConstraint1: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint2: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuLbl.text = "Афіша"
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ContainerVC.close), name:"menuDidSelected", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ContainerVC.checkWeb), name:"checkWeb", object: nil)
    }

    
    @IBAction func leftSwipe(sender: UISwipeGestureRecognizer) {
        open()
    }
    
    @IBAction func rightSwipe(sender: UISwipeGestureRecognizer) {
        close()
    }
    
    
    @IBAction func menuTapped(sender: UIButton) {
        if rightConstraint1.constant == 0 {
            open()
        } else {
            close()
        }
    }
    
    func open(){
        rightConstraint1.constant = menuView.frame.size.width
        rightConstraint2.constant = menuView.frame.size.width
        
        UIView.animateWithDuration(0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func close(notification: NSNotification = NSNotification(name: "justEmpty", object: nil)){

        if let obj = notification.object {
            let object = obj as! Int
            
            switch object {
            case 0:
                menuLbl.text = "Афіша"
            case 1:
                menuLbl.text = "Історія театру"
            case 2:
                menuLbl.text = "Художньо-керівний склад"
            case 3:
                menuLbl.text = "Творчий склад"
            case 4:
                menuLbl.text = "Адміністрація"
            case 5:
                menuLbl.text = "Квитки"
            case 6:
                menuLbl.text = "Контакти"
            default:
                break
            }
        }
       
        rightConstraint1.constant = 0
        rightConstraint2.constant = 0
        UIView.animateWithDuration(0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func checkWeb(){
        menuLbl.text = "Відсутній зв'язок!"
    }
}
