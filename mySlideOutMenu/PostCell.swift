//
//  PostCell.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage


class PostCell: UITableViewCell {
    
    @IBOutlet weak var myImgView: UIImageView!
    @IBOutlet weak var cellLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dwLbl: UILabel!
    @IBOutlet weak var dmLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myImgView.clipsToBounds = true
        myImgView.contentMode = UIViewContentMode.ScaleAspectFill
    }
    
    func configureCell(post: Post){
        cellLbl.text = post.header
        dmLbl.text = post.dm
        dwLbl.text = post.dw
        timeLbl.text = post.time        
    }
}