//
//  Extensions.swift
//  mySlideOutMenu
//
//  Created by user on 16/04/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class ExUILabel: UILabel {
    
    @IBInspectable var fontName: String = "Default-Font" {
        didSet {
            self.font = UIFont(name: fontName, size:self.font.pointSize)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.font = UIFont(name: fontName, size:self.font.pointSize)
    }
}


